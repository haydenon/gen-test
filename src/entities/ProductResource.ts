import {
  constrain,
  def,
  num,
  OutputValues,
  PropertiesBase,
  PropertyDefinition,
  PropertyValues,
  Resource,
  str,
  undefinable,
} from "@haydenon/gen";

class ProductProperties extends PropertiesBase {
  name: PropertyDefinition<string> = def(str());
  description: PropertyDefinition<string | undefined> = def(undefinable(str()));
  price: PropertyDefinition<number> = def(
    constrain(num(), { min: 0.5, max: 5000, precision: 0.01, float: true })
  );
}

class ProductOutputs extends ProductProperties {
  id: PropertyDefinition<number> = def(num());
}

interface Product {
  id: number;
  name: string;
  description: string | undefined;
  price: number;
}

class ProductResource extends Resource<ProductProperties, ProductOutputs> {
  constructor() {
    super(new ProductProperties(), new ProductOutputs());
  }

  async create(
    inputs: PropertyValues<ProductProperties>
  ): Promise<OutputValues<ProductOutputs>> {
    const { memberId, ...bodyInputs } = inputs;
    const body = JSON.stringify(bodyInputs);
    const response = await fetch(`http://localhost:8000/product`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body,
    });
    if (response.status >= 400) {
      throw new Error(
        `Failed to create product: '${await response.text()}', ${JSON.stringify(
          inputs,
          null,
          2
        )}`
      );
    }

    const product: Product = (await response.json()).data;
    return product;
  }
}

export default new ProductResource();
