import {
  PropertiesBase,
  PropertyDefinition,
  getLink,
  Resource,
  PropertyValues,
  OutputValues,
  def,
  array,
  complex,
  num,
  str,
  constrain,
  ResourceGroup,
  undefinable,
} from "@haydenon/gen";
import Member from "./MemberResource";
import ProductResource from "./ProductResource";
import ServiceResource from "./ServiceResource";

interface OrderItems {
  id: number;
  quantity: number;
  note: string | undefined;
}

interface OrderableProperties extends PropertiesBase {
  id: PropertyDefinition<number>;
  price: PropertyDefinition<number>;
}

const OrderableResources: ResourceGroup<OrderableProperties> = [
  ProductResource,
  ServiceResource,
];

class OrderBase extends PropertiesBase {
  memberId: PropertyDefinition<number> = def(getLink(Member, (m) => m.id));
  orderItems: PropertyDefinition<OrderItems[]> = def(
    constrain(
      array(
        complex<OrderItems>({
          id: getLink(OrderableResources, (i) => i.id),
          quantity: constrain(num(), { min: 1, max: 10 }),
          note: undefinable(constrain(str(), { minLength: 5, maxLength: 30 })),
        })
      ),
      { minItems: 1, maxItems: 5 }
    )
  );
  giftWrap: PropertyDefinition<string | undefined> = def(undefinable(str()));
}

class OrderOutputs extends OrderBase {
  id: PropertyDefinition<number> = def(num());
}

interface Order {
  id: number;
  memberId: number;
  giftWrap: string | undefined;
  orderItems: OrderItems[];
}

class OrderResource extends Resource<OrderBase, OrderOutputs> {
  async create(
    inputs: PropertyValues<OrderBase>
  ): Promise<OutputValues<OrderOutputs>> {
    const { memberId, ...bodyInputs } = inputs;
    const body = JSON.stringify(bodyInputs);
    const response = await fetch(
      `http://localhost:8000/member/${inputs.memberId}/order`,
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body,
      }
    );
    if (response.status >= 400) {
      throw new Error(
        `Failed to create order: '${await response.text()}', ${JSON.stringify(
          inputs,
          null,
          2
        )}`
      );
    }

    const order: Order = (await response.json()).data;
    return order;
  }
}
export default new OrderResource(new OrderBase(), new OrderOutputs());
