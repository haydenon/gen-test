import {
  constrain,
  def,
  num,
  OutputValues,
  PropertiesBase,
  PropertyDefinition,
  PropertyValues,
  Resource,
  str,
  undefinable,
} from "@haydenon/gen";

class ServiceProperties extends PropertiesBase {
  name: PropertyDefinition<string> = def(str());
  fulfiller: PropertyDefinition<string> = def(str());
  price: PropertyDefinition<number> = def(
    constrain(num(), { min: 0.5, max: 5000, precision: 0.01, float: true })
  );
}

class ServiceOutputs extends ServiceProperties {
  id: PropertyDefinition<number> = def(num());
}

interface Service {
  id: number;
  name: string;
  fulfiller: string;
  price: number;
}

class ServiceResource extends Resource<ServiceProperties, ServiceOutputs> {
  constructor() {
    super(new ServiceProperties(), new ServiceOutputs());
  }

  async create(
    inputs: PropertyValues<ServiceProperties>
  ): Promise<OutputValues<ServiceOutputs>> {
    const { memberId, ...bodyInputs } = inputs;
    const body = JSON.stringify(bodyInputs);
    const response = await fetch(`http://localhost:8000/service`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body,
    });
    if (response.status >= 400) {
      throw new Error(
        `Failed to create service: '${await response.text()}', ${JSON.stringify(
          inputs,
          null,
          2
        )}`
      );
    }

    const service: Service = (await response.json()).data;
    return service;
  }
}

export default new ServiceResource();
