import { faker } from "@faker-js/faker";
import {
  getLink,
  Resource,
  PropertyDefinition,
  createDepdendentConstraint as dependentConstraint,
  PropertiesBase,
  PropertyValues,
  OutputValues,
  def,
  constrain,
  str,
  num,
} from "@haydenon/gen";
import { Email } from "@haydenon/gen/inbuilt";

const FirstNameProperty: PropertyDefinition<string> = def(
  constrain(str(), {
    generateConstrainedValue: () => faker.name.firstName(),
  })
);

const LastNameProperty: PropertyDefinition<string> = def(
  constrain(str(), {
    generateConstrainedValue: () => faker.name.lastName(),
  })
);

class MemberBase extends PropertiesBase {
  email: PropertyDefinition<string> = def(
    constrain(
      getLink(Email, (e) => e.value),
      dependentConstraint<MemberBase, string>((values) =>
        faker.internet.exampleEmail(values.firstName, values.lastName)
      )
    )
  );
  firstName = FirstNameProperty;
  lastName = LastNameProperty;
}

class MemberOutputs extends MemberBase {
  id: PropertyDefinition<number> = def(num());
}

interface Member {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
}

class MemberResource extends Resource<MemberBase, MemberOutputs> {
  async create(
    inputs: PropertyValues<MemberBase>
  ): Promise<OutputValues<MemberOutputs>> {
    const body = JSON.stringify({ ...inputs });
    const response = await fetch("http://localhost:8000/member", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body,
    });
    if (response.status >= 400) {
      throw new Error(
        `Failed to create member ${JSON.stringify(inputs, null, 2)}`
      );
    }

    const member: Member = (await response.json()).data;
    return member;
  }
}

export default new MemberResource(new MemberBase(), new MemberOutputs());
