import { createDesiredState, Generator } from "@haydenon/gen";
import MemberResource from "./entities/MemberResource";
import OrderResource from "./entities/OrderResource";

async function run() {
  try {
    const generator = Generator.create(
      [
        createDesiredState(MemberResource, {}),
        createDesiredState(OrderResource, {}),
      ],
      {
        onCreate: (item) => console.log("Item created:", item),
        onError: (err) => console.error("Error creating item", err),
      }
    );
    const result = await generator.generateState();
    console.log("Result", result);
  } catch (err) {
    console.error(err);
  }
}

run();
