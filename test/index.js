const express = require("express");
const bodyParser = require("body-parser");
const { readFile, writeFile } = require("fs/promises");
const { readdirSync } = require("fs");

const app = express();
app.use(bodyParser.json());

const port = 8000;
app.listen(port, () => {
  console.log("We are live on " + port);
});

const idValues = {};

const getNextId = async (type) => {
  if (idValues[type]) {
    return ++idValues[type];
  }

  const files = readdirSync("./test/data").filter((file) =>
    file.startsWith(`${type}-`)
  );
  if (files.length <= 0) {
    return (idValues[type] = 1);
  }

  const ids = files
    .map((file) => {
      const parts = file.split("/");
      return parts[parts.length - 1];
    })
    .filter((file) => file.startsWith(type))
    .map((file) => {
      const parts = file.substring(0, file.length - 5).split("-");
      return parseInt(parts[1]);
    });

  const id = ids.sort((a, b) => a - b)[ids.length - 1] + 1;
  idValues[type] = id;
  return id;
};

app.post("/member", async (req, res) => {
  const nextId = await getNextId("member");
  const body = req.body;
  body.id = nextId;
  await writeFile(
    `./test/data/member-${nextId}.json`,
    JSON.stringify(body, null, 2)
  );
  res.send({
    success: true,
    message: "Successfully created member.",
    data: body,
  });
});

app.get("/member/:id", async (req, res) => {
  const id = parseInt(req.params.id);
  if (!id || isNaN(id)) {
    res.status(400);
    res.send({
      success: false,
      message: "Invalid member id.",
    });
    return;
  }

  try {
    const member = JSON.parse(
      (await readFile(`./test/data/member-${id}.json`)).toString()
    );
    res.send({
      success: true,
      message: "Successfully retrieved member.",
      data: member,
    });
  } catch {
    res.status(404);
    res.send({
      success: false,
      message: "Member not found.",
    });
  }
});

app.post("/member/:id/order", async (req, res) => {
  const id = parseInt(req.params.id);
  if (!id || isNaN(id)) {
    res.status(400);
    res.send({
      success: false,
      message: "Invalid member id.",
    });
    return;
  }

  let member;
  try {
    member = JSON.parse(
      (await readFile(`./test/data/member-${id}.json`)).toString()
    );
  } catch {
    res.status(404);
    res.send({
      success: false,
      message: "Member not found.",
    });
    return;
  }

  const nextId = await getNextId("order");
  const body = req.body;
  body.id = nextId;
  body.memberId = member.id;
  await writeFile(
    `./test/data/order-${nextId}.json`,
    JSON.stringify(body, null, 2)
  );
  res.send({
    success: true,
    message: "Successfully created order.",
    data: body,
  });
});

app.post("/product", async (req, res) => {
  const nextId = await getNextId("product");
  const body = req.body;
  body.id = nextId;
  await writeFile(
    `./test/data/product-${nextId}.json`,
    JSON.stringify(body, null, 2)
  );
  res.send({
    success: true,
    message: "Successfully created product.",
    data: body,
  });
});

app.post("/service", async (req, res) => {
  const nextId = await getNextId("service");
  const body = req.body;
  body.id = nextId;
  await writeFile(
    `./test/data/service-${nextId}.json`,
    JSON.stringify(body, null, 2)
  );
  res.send({
    success: true,
    message: "Successfully created service.",
    data: body,
  });
});
